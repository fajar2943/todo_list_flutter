import 'package:flutter/material.dart';
import 'package:todo_list/style.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: TodoPage(),
    );
  }
}

class TodoPage extends StatefulWidget {
  const TodoPage({
    super.key,
  });

  @override
  State<StatefulWidget> createState() => TodoState();
}

class TodoState extends State<TodoPage> {
  int counter = 0;
  List<String> todoList = [
    "Belajar Flutter",
    "Mengerjakan tugas kelompok",
    "Beli makan siang bareng",
  ];
  TextEditingController todoCtrl = TextEditingController();

  void tampilForm(var context) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) => Dialog(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Text(
                'Tambah aktivitas',
                style: header3,
              ),
              const SizedBox(height: 15),
              TextField(
                controller: todoCtrl,
                decoration: InputDecoration(hintText: "Todo baru"),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text('Close'),
                  ),
                  TextButton(
                    onPressed: () {
                      setState(() {
                        todoList.add(todoCtrl.text);
                        todoCtrl.text = '';
                        Navigator.pop(context);
                      });
                    },
                    child: const Text('Simpan'),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          tampilForm(context);
        },
        child: Icon(
          Icons.plus_one,
        ),
      ),
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Text(
              "List Pekerjaan",
              style: header1,
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: todoList.length,
              itemBuilder: (context, index) {
                return ListTile(
                  leading: Icon(Icons.check_circle_rounded),
                  title: Text(
                    todoList[index],
                    style: header4,
                  ),
                  // subtitle: Text("ini adalah subtitle"),
                  trailing: IconButton(
                    onPressed: () {
                      setState(() {
                        todoList.removeAt(index);
                      });
                    },
                    icon: Icon(
                      Icons.delete,
                      color: Colors.redAccent,
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
